'''
def myfun():  #defination
    print('hello world')    #body
myfun()   #call
print(myfun())   #none


def add():
    #print(2)
    return(2) #end of function
print(add()*100)

a=int(input('enter first number: '))
b=int(input('enter second number: '))
def add(x,y):
    return x+y
print(add(a,b))
'''
'''arguments for parametrised function
1. positional arguments
2.keyword arguments
3.default arguments
4.arbitary arguments
'''

def employee(name,salary):
    print('name:{} salary:{}/-'.format(name,salary))

#positional
employee('peter',20000)
employee(200000,'shinchan')

#keyword
employee(salary=200000,name='shinchan')
employee('shinchan',salary=2000)
#employee(salary=200000,'shinchan') #first positional then keyword
#employee('shinchan',name='peter') #incorrect

#default
def emp(name,salary=30000):  #first positional then keyword otherwise all keyword
    print(name,salary)
emp('param')
