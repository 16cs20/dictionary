
#filter ,pass function and iterator,iterator are multi string like hello, function true then iterator return true values in list,basically search
#map, pass same,one operation(function) apply on total list
#reduce, pass same,return singal value like add 10,20,30,it add first add 10 and 20 then result added with 30 and output is a singal value

#filter used in data science for web scraping
marks=[100,10,99,20,30,40,50,60,70,80,90,33]
def check(v):
    if v>=33:
        return True
res=list(filter(check,marks))
print(res)
print('total:',len(res))

res=list(filter(lambda marks:(marks>=33),marks))  #lambda
print(res)

comp=[i>33 for i in marks ]   #comparihansion
print(comp) 
comp=[i for i in marks if i>33]
print(comp)
comp=[i for i in range(3)]
print(comp) 
comp=[i for i in range(10) if i%2==0]
print(comp) 
comp=[i for i in marks if i%2==0]
print(comp) 

#map
ls=['Uff','dTd','sRs','gEg','ttQ','wWw','Sve']
marks=[100,20,30,40,56,78]
def check(st):
    return st.title()  #lower(),upper()
def perc(n):
    p=(n/120)*100
    return round(p,3) 
res=list(map(check,ls))
print(res)

perce=list(map(perc,marks))
print(perce,marks)

res=list(map(lambda a:a.title(),ls))
print(res)
res=list(map(lambda n:(n/120)*100,marks))
print(res)

#reduce
from functools import reduce
marks=[23,45,67,8,9,23,89,87,56,100]
res=reduce(lambda x,y:x+y,marks)

total=len(marks)*100
print((res/total)*100)

max=reduce(lambda a,b:a if a>b else b,marks)
print(max)